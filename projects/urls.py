from django.urls import path
from projects.views import (
    projects_list,
    projects_list_detail,
    create_project,
)

urlpatterns = [
    path("", projects_list, name="list_projects"),
    path("projects/<int:id>/", projects_list_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
